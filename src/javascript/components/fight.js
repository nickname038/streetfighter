import { controls } from '../../constants/controls';

let fighters = Array(2);
let winner;
let pressed = new Set();

export async function fight(firstFighter, secondFighter) {
  const indicators = [document.getElementById('left-fighter-indicator'), document.getElementById('right-fighter-indicator')];
  
  for (let i = 0; i < fighters.length; i++) {
    fighters[i] = {...[firstFighter, secondFighter][i]};
    fighters[i].startHealth = fighters[i].health;
    fighters[i].isInBlock = false;
    fighters[i].isCanStrikeCriticalHit = true;
    fighters[i].indicator = indicators[i];
  }

  document.addEventListener('keydown', onKeyDown);
  document.addEventListener('keyup', onKeyUp);
  
  return new Promise(async (resolve) => { 
    let timerId = setTimeout(function tick() {
      if (winner) {

        document.removeEventListener('keydown', onKeyDown);
        document.removeEventListener('keyup', onKeyUp);

        resolve(winner);
        clearTimeout(timerId);

        return;
      }
      timerId = setTimeout(tick, 1000);
    }, 1000);
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const power = getHitPower(attacker) - getBlockPower(defender);
  return (power > 0) ? power : 0;
  // return damage
}

export function getHitPower(fighter) {
  const criticalHitChance = GetRandomFloat(1, 2);
  const power = fighter.attack * criticalHitChance;
  return power;
  // return hit power
}

export function getBlockPower(fighter) {
  if (!fighter.isInBlock) return 0;
  const dodgeChance = GetRandomFloat(1, 2);
  const power = fighter.defense * dodgeChance;
  return power;
  // return block power
}

function getCriticalPower(attacker) {
  attacker.isCanStrikeCriticalHit = false;
  setTimeout(() => attacker.isCanStrikeCriticalHit = true, 10000);
  const power = 2 * attacker.attack;
  return power;
}

function onKeyDown(event) {

  switch (event.code) {
    case controls.PlayerOneAttack:
      onAttack(fighters[0], fighters[1], false);
      return;
    case controls.PlayerTwoAttack:
      onAttack(fighters[1], fighters[0], false);
      return;
    case controls.PlayerOneBlock:
      onChangeBlockState(fighters[0], true);
      return;
    case controls.PlayerTwoBlock:
      onChangeBlockState(fighters[1], true);
      return;
    default:
      pressed.add(event.code);
      break;
  }

  if (checkCodeShortcuts(controls.PlayerOneCriticalHitCombination)) {
    onAttack(fighters[0], fighters[1], true);
  } else if (checkCodeShortcuts(controls.PlayerTwoCriticalHitCombination)) {
    onAttack(fighters[1], fighters[0], true);
  }

}

function onKeyUp(event) {
  switch (event.code) {
    case controls.PlayerOneBlock:
      onChangeBlockState(fighters[0], false);
      break;
    case controls.PlayerTwoBlock:
      onChangeBlockState(fighters[1], false);
      break;
    default:
      break;
  }
  pressed.delete(event.code);
}

function checkCodeShortcuts(codeShortcut) {

  for (let code of codeShortcut) {
    if (!pressed.has(code)) {
      return false;
    }
  }

  pressed.clear();
  return true;
}

function GetRandomFloat(min, max) {
  let result = Math.random() * (max - min);
  result += min;
  return result;
}

function onChangeBlockState(fighter, value) {
  fighter.isInBlock = value;
}

function onAttack(attacker, defender, isCriticalHit) {
  let damage;

  if (isCriticalHit) {
    if (attacker.isCanStrikeCriticalHit) {
      damage = getCriticalPower(attacker);
    } else {
      return;
    }
  } else {
    damage = getDamage(attacker, defender);
  }

  updatedHalth(defender, damage);
  checkGameStatus(attacker, defender);
}

function updatedHalth(defender, damage) {
  const updatedHalth = defender.health - damage;
  defender.health = (updatedHalth < 0) ? 0 : updatedHalth;
  defender.indicator.style.width = `${defender.health * 100 / defender.startHealth}%`;
}

function checkGameStatus(attacker, defender) {
  if (defender.health == 0) {
    winner = attacker;
  }
}
