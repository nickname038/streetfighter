import { showModal } from './modal'
import { createElement } from '../../helpers/domHelper';
import { createImage } from '../fightersView'

export function showWinnerModal(fighter) {
  const element = createElement({ tagName: 'div', className: 'results' });
  element.append(createImage(fighter));
  showModal({ title: `Winner: ${fighter.name.toUpperCase()}`, bodyElement: element, onClose: () => document.location.reload() });
  // call showModal function 
}
