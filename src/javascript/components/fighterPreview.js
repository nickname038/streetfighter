import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) return fighterElement;

  fighterElement.innerHTML = `
  <div class="information">
  <h1>${fighter.name}</h1>
  <p><strong>Health:</strong> ${fighter.health}</p>
  <p><strong>Attack:</strong> ${fighter.attack}</p>
  <p><strong>Defense:</strong> ${fighter.defense}</p>
  </div>
  `;

  const fighterPreviewImageWrapper = createElement({
    tagName: 'div',
    className: `fighter-preview-image-wrapper`,
  });

  fighterPreviewImageWrapper.append(createFighterImage(fighter));
  fighterElement.prepend(fighterPreviewImageWrapper);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
